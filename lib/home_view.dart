import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:pedometer/pedometer.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() {
    // TODO: implement createState
    _HomeViewState();
  }

}

class _HomeViewState extends State<HomeView> {

  String _km = "unknown";

  String _stepCountValue = "unknown";
  StreamSubscription<int> _subscription;

  double _stepCounter = 0;
  double _convert;
  double _kmx;
  double _current;

  // Daily Goal
  double _goal = 15000.0;

  @override
  void initState() {
    super.initState();
    setUpPedometer();
  }

  void setUpPedometer() {
    Pedometer pedometer = new Pedometer();
    _subscription = pedometer.pedometerStream.listen(_onData,
        onError: _onError, onDone: _onDone, cancelOnError: true);
  }

  void _onDone() {

  }

  void _onError(error) {
    print("Flutter Pedometer Fehler: $error");
  }

  void _onData(int stepCountValue) async{
    setState(() {
      _stepCountValue = "$stepCountValue";
      print(_stepCountValue);
    });

    var distance = stepCountValue;
    double y = (distance + .0); //

    setState(() {
      _stepCounter = y;

      _current = _goal / y;
    });

    //
    var long3 = (_stepCounter );
    long3 = num.parse(y.toStringAsFixed(2));
    var long4 = (long3 / 10000);

    int decimals = 1;
    int fac = pow(10, decimals);
    double d = long4;
    d = (d * fac).round() / fac;
    print("d: $d");

    getDistance(_stepCounter);



    setState(() {
      _convert = d;
      print('test '+ '$_convert' );
    });
  }

  void reset(){
    setState(() {
      int stepCountValue = 0;
      stepCountValue = 0;
      _stepCountValue = "$stepCountValue";
    });
  }

  //get distance in kilometer
  void getDistance(double _stepCounter){
    var distance = ((_stepCounter * 70) / 100000);
    distance = num.parse(distance.toStringAsFixed(2));
    var distancekmx = distance * 34;
    distancekmx = num.parse(distancekmx.toStringAsFixed(2));
    setState(() {
      _km = "$distance";
      print('Zeige Kilometer '+ _km);
    });
    setState(() {
      _kmx = num.parse(distancekmx.toStringAsFixed(2));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: new ListView(
          padding: EdgeInsets.all(5.0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 15.0),
              width: 250,
              height: 250,
              child: new CircularPercentIndicator(
                radius: 200.0,
                lineWidth: 15.0,
                animation: true,
                center: Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          '$_stepCountValue',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25.0,
                              color: Colors.blue),
                        ),
                      ),
                    ],
                  ),
                ),
                percent:  .65,
                circularStrokeCap: CircularStrokeCap.round,
                progressColor: Colors.blue,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 2.0),
              height: 50,
              color: Colors.transparent,
              child: Row(
                children: <Widget>[
                  new Container(
                      padding: EdgeInsets.only(left: 40.0),

                      child: Container(
                        child: Text(
                          "$_km Km",
                          textAlign: TextAlign.right,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                              color: Colors.black),
                        ),
                      )
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Container(
                      child: Text(
                        "$_stepCountValue Steps",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                            color: Colors.black),
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Container(
                      child: Text(
                        "$DateTime",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}