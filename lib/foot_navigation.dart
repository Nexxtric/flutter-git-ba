import 'package:ba_flutter_health_app/home_view.dart';
import 'package:ba_flutter_health_app/pages.dart';
import 'package:flutter/material.dart';
import 'package:ba_flutter_health_app/views/settings.dart';
import 'package:ba_flutter_health_app/views/who.dart';


class HomeFootNavigation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeFootNavigationState();
  }

}

class _HomeFootNavigationState extends State<HomeFootNavigation>{

  int _currentIndex = 0;
  final List<Widget> _children = [
    TestRun(),
    HistoryPage(),
    WHO(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter eHealth App",
          style: TextStyle(
            color: Colors.blue
          ),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            color: Colors.blue,
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));
            },
          )
        ],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.blue,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(
                Icons.favorite,
            ),
            title: new Text("Home"),
          ),
          BottomNavigationBarItem(
              icon: new Icon(
                  Icons.photo_camera
              ),
              title: new Text("History"),

          ),
          BottomNavigationBarItem(
              icon: new Icon(
                  Icons.web,
              ),
              title: new Text("WHO"),
          ),
        ]
      ),
    );
  }

  void onTabTapped(int index){
    setState(() {
      _currentIndex = index;
    });
  }

}