import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Settings extends StatelessWidget{
  double _goal;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.blue
        ),
        title: Text("Einstellungen",
          style: TextStyle(
            color: Colors.blue
          ),
        ),

        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            color: Colors.red,
          )
        ],

      ),
      // implementation des Body für Einstellungen
      body: new Container(
        color: Colors.red,
      ),
    );
  }
}