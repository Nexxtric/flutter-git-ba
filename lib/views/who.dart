import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WHO extends StatefulWidget{
  @override
  _WHOState createState() {
    return _WHOState();
  }

}

class _WHOState extends State {
  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(url: 'https://www.who.int/health-topics/digital-health#tab=tab_2');
  }

}