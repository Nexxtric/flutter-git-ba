import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:pedometer/pedometer.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


import 'package:image_picker/image_picker.dart';
import 'dart:io';

class TestRun extends StatefulWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightGreenAccent,
    );
  }

  @override
  _TestRunState createState() {
    // TODO: implement createState
    return _TestRunState();
  }
}

class _TestRunState extends State{

  String _km = "0";

  String _stepCountValue = "0";

  StreamSubscription<int> _subscription;

  double _stepCounter = 0;
  double _convert;
  double _kmx;
  double _current;
  double _percent = 0.0;

  TimeOfDay now = TimeOfDay.now();

  // Daily Goal
  double _goal = 30000.0;

  @override
  void initState() {
    super.initState();
    setUpPedometer();
  }

  void setUpPedometer() {
    Pedometer pedometer = new Pedometer();
    _subscription = pedometer.pedometerStream.listen(_onData,
        onError: _onError, onDone: _onDone, cancelOnError: true);
  }

  void _onDone() {

  }

  void _onError(error) {
    print("Flutter Pedometer Fehler: $error");
  }

  void _onData(int stepCountValue) async{
    setState(() {
      _stepCountValue = "$stepCountValue";
    });

    var distance = stepCountValue;
    double y = (distance + .0); //

    setState(() {
      _stepCounter = y;
      _current = _goal / y;
    });

    var long3 = (_stepCounter );
    long3 = num.parse(y.toStringAsFixed(2));
    var long4 = (long3 / 10000);
    int decimals = 1;
    int fac = pow(10, decimals);
    double d = long4;
    d = (d * fac).round() / fac;
    getDistance(_stepCounter);



    setState(() {
      _convert = d;
    });

    if(now == TimeOfDay(hour: 0, minute: 0) ){
      reset();

    }

    _percent = 1-((30000/_stepCounter)-1);
  }

  void reset(){
    setState(() {
      int stepCountValue = 0;
      stepCountValue = 0;
      _stepCountValue = "$stepCountValue";
      double _stepCounter = 0.0;
      _km = '0';
      _kmx = 0.0;
      _percent = 0.0;
      _subscription.cancel();
    });
  }

  //get distance in kilometer
  void getDistance(double _stepCounter){
    var distance = ((_stepCounter * 70) / 100000);
    distance = num.parse(distance.toStringAsFixed(2));
    var distancekmx = distance * 34;
    distancekmx = num.parse(distancekmx.toStringAsFixed(2));
    setState(() {
      _km = "$distance";
    });
    setState(() {
      _kmx = num.parse(distancekmx.toStringAsFixed(2));
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: new ListView(
          padding: EdgeInsets.all(5.0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 15.0),
              width: 250,
              height: 250,
              child: new CircularPercentIndicator(
                radius: 200.0,
                lineWidth: 15.0,
                animation: true,
                center: Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          '$_stepCountValue',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25.0,
                              color: Colors.blue),
                        ),
                      ),
                    ],
                  ),
                ),
                percent: _percent,
                circularStrokeCap: CircularStrokeCap.round,
                progressColor: Colors.blue,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 2.0),
              height: 50,
              color: Colors.transparent,
              child: Row(
                children: <Widget>[
                  new Container(
                      padding: EdgeInsets.only(left: 40.0),

                      child: Container(
                        child: Text(
                          "$_km Km",
                          textAlign: TextAlign.right,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                              color: Colors.black),
                        ),
                      )
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Container(
                      child: Text(
                        "$_stepCountValue Steps",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                            color: Colors.black),
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Container(
                      child: Text(
                        "$DateTime",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}

class HistoryPage extends StatefulWidget {

  @override
  _HistoryPageState createState() {
    return _HistoryPageState();
  }
}

class _HistoryPageState extends State{
  //Firebase
  final db = Firestore.instance;

  // Kamera Anbindung
  File _image;

  Future getImage(bool isCamera) async{
    File image;
    if(isCamera){
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    }

    setState(() {
      _image = image;
    });
  }

  // Eintrag
  final List<String> historyList = [];


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: new ListView(
          children: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.insert_drive_file,
                ),
                onPressed: () {
                  getImage(false);
                },
            ),
            SizedBox(height: 10.0,),
            IconButton(
              icon: Icon(Icons.camera_alt),
              onPressed: () async {
                getImage(true);
                await db.collection("FotoHistory").add(
                  {
                    'title': "test",
                    'data': _image,
                  }
                );
              },
            ),
            _image == null? Container() : Image.file(_image, height: 300.0,width: 300.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Datum: "),

              ],
            ),

            //save data to firebase
          ],

        ),

      ),
    );
  }
}
